<?php
include('config.php')
?>

<!DOCTYPE HTML>
<!--
	Verti 2.5 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>A savoir - Connexion</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,800" rel="stylesheet" type="text/css" />
		<link href="http://fonts.googleapis.com/css?family=Oleo+Script:400" rel="stylesheet" type="text/css" />
		<script src="js/jquery.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
		<!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css" /><![endif]-->
	</head>
	<body class="no-sidebar">

		<!-- Header Wrapper -->
			<div id="header-wrapper">
				<div class="container">
					<div class="row">
						<div class="12u">
						
							<!-- Header -->
								<header id="header">
								
									<!-- Logo -->
										<div id="logo">
										<h1><a href="index.php" id="logo">A savoir</a></h1>
											<span>Tu crois tout connaitre ?</span>
										</div>
									
									<!-- Nav -->
									<nav id="nav">
											<ul>

												<li><a href="index.php">Les Savoirs</a></li>

												<?php
//On affiche un message de bienvenue, si lutilisateur est connecte, on affiche son pseudo
?>
<?php
//Si lutilisateur est connecte, on lui donne un lien pour modifier ses informations, pour voir ses messages et un pour se deconnecter
if(isset($_SESSION['username']))
{
?>
<li><a href="edit_infos.php">Mon profil</a></li>
            <li><a href="add.php">Ajouter un savoir</a></li>
<li  class="current_page_item" ><a href="connexion.php">Se d&eacute;connecter</a></li>
<?php
}
else
{
//Sinon, on lui donne un lien pour sinscrire et un autre pour se connecter
?>
<li><a href="sign_up.php">Inscription</a></li>
<li  class="current_page_item"><a href="connexion.php">Se connecter</a></li>
<?php
}
?>
												
											</ul>
										</nav>
								
								</header>

						</div>
					</div>
				</div>
			</div>
		
		<!-- Main Wrapper -->
			<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<div class="12u skel-cell-important">

							<!-- Content -->
								<div id="content">
									<article class="last">
<?php
//Si lutilisateur est connecte, on le deconecte
if(isset($_SESSION['username']))
{
        //On le deconecte en supprimant simplement les sessions username et userid
        unset($_SESSION['username'], $_SESSION['userid']);
        
?>
<div class="message">Vous avez bien &eacute;t&eacute; d&eacute;connect&eacute;.<br />

<script>setTimeout('window.location.href="index.php"', 1000); </script>
<?php
}
else
{
        $ousername = '';
        //On verifie si le formulaire a ete envoye
        if(isset($_POST['username'], $_POST['password']))
        {
                //On echappe les variables pour pouvoir les mettre dans des requetes SQL
                if(get_magic_quotes_gpc())
                {
                        $ousername = stripslashes($_POST['username']);
                        $username = mysql_real_escape_string(stripslashes($_POST['username']));
                        $password = stripslashes($_POST['password']);
                }
                else
                {
                        $username = mysql_real_escape_string($_POST['username']);
                        $password = $_POST['password'];
                }
                //On recupere le mot de passe de lutilisateur
                $req = mysql_query('select password,id from users where username="'.$username.'"');
                $dn = mysql_fetch_array($req);
                //On le compare a celui quil a entre et on verifie si le membre existe
                if($dn['password']==$password and mysql_num_rows($req)>0)
                {
                        //Si le mot de passe es bon, on ne vas pas afficher le formulaire
                        $form = false;
                        //On enregistre son pseudo dans la session username et son identifiant dans la session userid
                        $_SESSION['username'] = $_POST['username'];
                        $_SESSION['userid'] = $dn['id'];
?>
<div class="message">Vous avez bien &eacute;t&eacute; connect&eacute;. <br />
<script>setTimeout('window.location.href="index.php"', 1000); </script>
<?php
                }
                else
                {
                        //Sinon, on indique que la combinaison nest pas bonne
                        $form = true;
                        $message = 'La combinaison que vous avez entr&eacute; n\'est pas bonne.';
                }
        }
        else
        {
                $form = true;
        }
        if($form)
        {
                //On affiche un message sil y a lieu
        if(isset($message))
        {
                echo '<div class="message">'.$message.'</div>';
        }
        //On affiche le formulaire
?>
<div class="content">
    <form action="connexion.php" method="post">
        Veuillez entrer vos identifiants pour vous connecter:<br />
        <div class="center">
            <label for="username">Nom d'utilisateur</label><input type="text" name="username" id="username" value="<?php echo htmlentities($ousername, ENT_QUOTES, 'UTF-8'); ?>" /><br />
            <label for="password">Mot de passe</label><input type="password" name="password" id="password" /><br />
            <input type="submit" value="Connection" />
                </div>
    </form>
</div>
<?php
        }
}
?>

									</article>
								</div>

						</div>
					</div>
				</div>
			</div>

		
		<!-- Footer Wrapper -->
			<div id="footer-wrapper">
				<footer id="footer" class="container">
					<div class="row">
					
						<div class="3u">
						
							<!-- Contact -->
								<section class="widget-contact last">
									<h2>Contact Us</h2>
									<ul>
										<li><a href="#" class="fa fa-twitter solo"><span>Twitter</span></a></li>
										<li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
										<li><a href="#" class="fa fa-dribbble solo"><span>Dribbble</span></a></li>
										<li><a href="#" class="fa fa-google-plus solo"><span>Google+</span></a></li>
									</ul>
									<p>20 rue Merlin de Thionville<br />
									92150, Suresnes<br />
									0647464176</p>
								</section>
						
						</div>
					</div>
					<div class="row">
						<div class="12u">
							<div id="copyright">
								|&copy; A savoir. All rights reserved. | 
							</div>
						</div>
					</div>
				</footer>
			</div>

	</body>
</html>