
<?php
include('config.php')
?>

<!DOCTYPE HTML>
<!--
        Verti 2.5 by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
        <head>
                <title>A savoir - Contact</title>
                <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                <meta name="description" content="" />
                <meta name="keywords" content="" />
                <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,800" rel="stylesheet" type="text/css" />
                <link href="http://fonts.googleapis.com/css?family=Oleo+Script:400" rel="stylesheet" type="text/css" />
                <script src="js/jquery.min.js"></script>
                <script src="js/config.js"></script>
                <script src="js/skel.min.js"></script>
                <script src="js/skel-panels.min.js"></script>
                <noscript>
                        <link rel="stylesheet" href="css/skel-noscript.css" />
                        <link rel="stylesheet" href="css/style.css" />
                        <link rel="stylesheet" href="css/style-desktop.css" />
                </noscript>
                <!--[if lte IE 8]><script src="js/html5shiv.js"></script><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
                <!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css" /><![endif]-->
        </head>
        <body class="no-sidebar">

                <!-- Header Wrapper -->
                        <div id="header-wrapper">
                                <div class="container">
                                        <div class="row">
                                                <div class="12u">
                                                
                                                        <!-- Header -->
                                                                <header id="header">
                                                                
                                                                        <!-- Logo -->
                                                                                <div id="logo">
                                                                                <h1><a href="index.php" id="logo">A savoir</a></h1>
                                                                                        <span>Tu crois tout connaitre ?</span>
                                                                                </div>
                                                                        
                                                                        <!-- Nav -->
                                                                        <nav id="nav">
                                                                                        <ul>


                                                                                                <?php
//On affiche un message de bienvenue, si lutilisateur est connecte, on affiche son pseudo
?>
<?php
//Si lutilisateur est connecte, on lui donne un lien pour modifier ses informations, pour voir ses messages et un pour se deconnecter
if(isset($_SESSION['username']))
{
?>
<li class="current_page_item"><a href="contact.php">Contact</a></li>
<li><a href="edit_infos.php">Mon profil</a></li>
            <li><a href="add.php">Ajouter un savoir</a></li>
<li><a href="connexion.php">Se d&eacute;connecter</a></li>
<?php
}
else
{
//Sinon, on lui donne un lien pour sinscrire et un autre pour se connecter
?>
<li><a href="sign_up.php">Inscription</a></li>
<li><a href="connexion.php">Se connecter</a></li>
<?php
}
?>
                                                                                                
                                                                                        </ul>
                                                                                </nav>
                                                                
                                                                </header>

                                                </div>
                                        </div>
                                </div>
                        </div>
                
                <!-- Main Wrapper -->
                        <div id="main-wrapper">
                                <div class="container">
                                        <div class="row">
                                                <div class="12u skel-cell-important">

                                                        <!-- Content -->
                                                                <div id="content">



<?php
/*
	********************************************************************************************
	CONFIGURATION
	********************************************************************************************
*/
// destinataire est votre adresse mail. Pour envoyer à plusieurs à la fois, séparez-les par une virgule
$destinataire = 'dauteuillestephane@gmail.com';

// copie ? (envoie une copie au visiteur)
$copie = 'non';

// Action du formulaire (si votre page a des paramètres dans l'URL)
// si cette page est index.php?page=contact alors mettez index.php?page=contact
// sinon, laissez vide
$form_action = '';

// Messages de confirmation du mail
$message_envoye = "Votre message nous est bien parvenu !";
$message_non_envoye = "L'envoi du mail a échoué, veuillez réessayer SVP.";

// Message d'erreur du formulaire
$message_formulaire_invalide = "Vérifiez que tous les champs soient bien remplis et que l'email soit sans erreur.";

/*
	********************************************************************************************
	FIN DE LA CONFIGURATION
	********************************************************************************************
*/

/*
 * cette fonction sert à nettoyer et enregistrer un texte
 */
function Rec($text)
{
	$text = htmlspecialchars(trim($text), ENT_QUOTES);
	if (1 === get_magic_quotes_gpc())
	{
		$text = stripslashes($text);
	}

	$text = nl2br($text);
	return $text;
};

/*
 * Cette fonction sert à vérifier la syntaxe d'un email
 */
function IsEmail($email)
{
	$value = preg_match('/^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!\.)){0,61}[a-zA-Z0-9_-]?\.)+[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!$)){0,61}[a-zA-Z0-9_]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/', $email);
	return (($value === 0) || ($value === false)) ? false : true;
}

// formulaire envoyé, on récupère tous les champs.
$nom     = (isset($_POST['nom']))     ? Rec($_POST['nom'])     : '';
$email   = (isset($_POST['email']))   ? Rec($_POST['email'])   : '';
$objet   = (isset($_POST['objet']))   ? Rec($_POST['objet'])   : '';
$message = (isset($_POST['message'])) ? Rec($_POST['message']) : '';

// On va vérifier les variables et l'email ...
$email = (IsEmail($email)) ? $email : ''; // soit l'email est vide si erroné, soit il vaut l'email entré
$err_formulaire = false; // sert pour remplir le formulaire en cas d'erreur si besoin

if (isset($_POST['envoi']))
{
	if (($nom != '') && ($email != '') && ($objet != '') && ($message != ''))
	{
		// les 4 variables sont remplies, on génère puis envoie le mail
		$headers  = 'From:'.$nom.' <'.$email.'>' . "\r\n";
		//$headers .= 'Reply-To: '.$email. "\r\n" ;
		//$headers .= 'X-Mailer:PHP/'.phpversion();

		// envoyer une copie au visiteur ?
		if ($copie == 'oui')
		{
			$cible = $destinataire.','.$email;
		}
		else
		{
			$cible = $destinataire;
		};

		// Remplacement de certains caractères spéciaux
		$message = str_replace("&#039;","'",$message);
		$message = str_replace("&#8217;","'",$message);
		$message = str_replace("&quot;",'"',$message);
		$message = str_replace('<br>','',$message);
		$message = str_replace('<br />','',$message);
		$message = str_replace("&lt;","<",$message);
		$message = str_replace("&gt;",">",$message);
		$message = str_replace("&amp;","&",$message);

		// Envoi du mail
		if (mail($cible, $objet, $message, $headers))
		{
			echo '<p>'.$message_envoye.'</p>';
		}
		else
		{
			echo '<p>'.$message_non_envoye.'</p>';
		};
	}
	else
	{
		// une des 3 variables (ou plus) est vide ...
		echo '<p>'.$message_formulaire_invalide.'</p>';
		$err_formulaire = true;
	};
}; // fin du if (!isset($_POST['envoi']))

if (($err_formulaire) || (!isset($_POST['envoi'])))
{
	// afficher le formulaire
	echo '
	<form id="contact" method="post" action="'.$form_action.'">
	<fieldset><legend>Vos coordonnées</legend>
		<p><label for="nom">Nom :</label><input type="text" id="nom" name="nom" value="'.stripslashes($nom).'" tabindex="1" /></p>
		<p><label for="email">Email :</label><input type="text" id="email" name="email" value="'.stripslashes($email).'" tabindex="2" /></p>
	</fieldset>

	<fieldset><legend>Votre message :</legend>
		<p><label for="objet">Objet :</label><input type="text" id="objet" name="objet" value="'.stripslashes($objet).'" tabindex="3" /></p>
		<p><label for="message">Message :</label><textarea id="message" name="message" tabindex="4" cols="30" rows="8">'.stripslashes($message).'</textarea></p>
	</fieldset>

	<div style="text-align:center;"><input type="submit" name="envoi" value="Envoyer le formulaire !" /></div>
	</form>';
};
?>


      </article>
                                                                </div>

                                                </div>
                                        </div>
                                </div>
                        </div>

                <!-- Footer Wrapper -->
                        <div id="footer-wrapper">
                                <footer id="footer" class="container">
                                        <div class="row">
                                                
                                                <div class="3u">
                                                
                                                        <!-- Contact -->
                                                                <section class="widget-contact last">
                                                                        <h2>Contact Us</h2>
                                                                        <ul>
                                                                                <li><a href="#" class="fa fa-twitter solo"><span>Twitter</span></a></li>
                                                                                <li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
                                                                                <li><a href="#" class="fa fa-dribbble solo"><span>Dribbble</span></a></li>
                                                                                <li><a href="#" class="fa fa-google-plus solo"><span>Google+</span></a></li>
                                                                        </ul>
                                                                        <p>1234 Fictional Road Suite #5432<br />
                                                                        Nashville, Tennessee 00000-0000<br />
                                                                        (800) 555-0000</p>
                                                                </section>
                                                
                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="12u">
                                                        <div id="copyright">
                                                                &copy; Untitled. All rights reserved. | Images: <a href="http://fotogrph.com/">fotogrph</a> | Design: <a href="http://html5up.net/">HTML5 UP</a>
                                                        </div>
                                                </div>
                                        </div>
                                </footer>
                        </div>

        </body>
</html>
