<?php
include('config.php')
?>

<!DOCTYPE HTML>
<!--
	Verti 2.5 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>A savoir - Administration</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,800" rel="stylesheet" type="text/css" />
		<link href="http://fonts.googleapis.com/css?family=Oleo+Script:400" rel="stylesheet" type="text/css" />
		<script src="js/jquery.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
		<!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css" /><![endif]-->
	</head>
	<body class="no-sidebar">

		<!-- Header Wrapper -->
			<div id="header-wrapper">
				<div class="container">
					<div class="row">
						<div class="12u">
						
							<!-- Header -->
								<header id="header">
								
									<!-- Logo -->
										<div id="logo">
										<h1><a href="index.php" id="logo">A savoir</a></h1>
											<span>Tu crois tout connaitre ?</span>
										</div>
									
									<!-- Nav -->
									<nav id="nav">
											<ul>


												<?php
//On affiche un message de bienvenue, si lutilisateur est connecte, on affiche son pseudo
?>
<?php
//Si lutilisateur est connecte, on lui donne un lien pour modifier ses informations, pour voir ses messages et un pour se deconnecter
if(isset($_SESSION['username']))
{
?>
<li class="current_page_item"><a href="nimda.php">Administration</a></li>
<li><a href="edit_infos.php">Mon profil</a></li>
            <li><a href="add.php">Ajouter un savoir</a></li>
<li><a href="connexion.php">Se d&eacute;connecter</a></li>
<?php
}
else
{
//Sinon, on lui donne un lien pour sinscrire et un autre pour se connecter
?>
<li><a href="sign_up.php">Inscription</a></li>
<li><a href="connexion.php">Se connecter</a></li>
<?php
}
?>
												
											</ul>
										</nav>
								
								</header>

						</div>
					</div>
				</div>
			</div>
		
		<!-- Main Wrapper -->
			<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<div class="12u skel-cell-important">

							<!-- Content -->
								<div id="content">

<title>Liste des utilisateurs</title>

       <h2>Administration:</h2><br />



Voici la liste des utilisateurs: <br> <br>

<?php

//On recupere les identifiants, les pseudos et les emails des utilisateurs
$req = mysql_query('select id, username, email from users');

$id=$_SESSION['userid']; 
while($dnn = mysql_fetch_array($req))
{

if ( $id == 1  ){  
 
?>
       <div style="background:#0090C5;padding:10px;width:auto;float:left;color:white;border-radius: 6px; margin-left:5px;  word-wrap: break-word;">

       
   <?php echo htmlentities($dnn['username'], ENT_QUOTES, 'UTF-8'); ?>

    <a style="border-radius: 6px;
    font-weight: 800;
    letter-spacing: 0.025em;
    padding: 0.5em 1em;
     background: none repeat scroll 0 0 #444444;
    color: #FFFFFF;
    text-decoration: none;
    transition: background-color 0.25s ease-in-out 0s;"
    href="profile.php?id=<?php echo $dnn['id']; ?>">Voir</a>
    <a style="border-radius: 6px;
    font-weight: 800;
    letter-spacing: 0.025em;
    padding: 0.5em 1em;
     background: none repeat scroll 0 0 #444444;
    color: #FFFFFF;
    text-decoration: none;
    transition: background-color 0.25s ease-in-out 0s;" href="delete_users.php?id=<?php echo $dnn['id']; ?> " >X</a>

    </div>
<?php
}else { 
header('Location: index.php');}}
?>
<br> <br>
<br> <br>

<div style="clear:both;"></div>

Voici la liste des savoirs: <br> <br>

<?php
//On recupere les identifiants, les pseudos et les emails des utilisateurs
$req = mysql_query('select id, link, text from savoir');
$id=$_SESSION['userid']; 

while($dnn = mysql_fetch_array($req))
{

	if ( $id == 1  ){  
?>
       
       <div style="background:#FF4486;padding:10px;color:white;border-radius: 6px;  word-wrap: break-word;">
                <img src="<?php echo "upload/".$dnn['link']; ?>" width="50" > 

           

        <?php echo htmlentities($dnn['text'], ENT_QUOTES, 'UTF-8'); ?><br>
        <a style="border-radius: 6px;
    font-weight: 800;
    letter-spacing: 0.025em;
    padding: 0.5em 1em;
     background: none repeat scroll 0 0 #444444;
    color: #FFFFFF;
    text-decoration: none;
    transition: background-color 0.25s ease-in-out 0s;"href="voir_savoir.php?id=<?php echo $dnn['id']; ?> " >Voir</a>&nbsp;<a style="border-radius: 6px;
    font-weight: 800;
    letter-spacing: 0.025em;
    padding: 0.5em 1em;
     background: none repeat scroll 0 0 #444444;
    color: #FFFFFF;
    text-decoration: none;
    transition: background-color 0.25s ease-in-out 0s;"href="edit_savoir.php?id=<?php echo $dnn['id']; ?> " >Edit</a>&nbsp;<a style="border-radius: 6px;
    font-weight: 800;
    letter-spacing: 0.025em;
    padding: 0.5em 1em;
     background: none repeat scroll 0 0 #444444;
    color: #FFFFFF;
    text-decoration: none;
    transition: background-color 0.25s ease-in-out 0s;" href="delete_savoir.php?id=<?php echo $dnn['id']; ?> " >X</a>&nbsp;<br></div>
    
<?php
}}
?>


									</article>
								</div>

						</div>
					</div>
				</div>
			</div>

		<!-- Footer Wrapper -->
			<div id="footer-wrapper">
				<footer id="footer" class="container">
					<div class="row">
						
						<div class="3u">
						
							<!-- Contact -->
								<section class="widget-contact last">
									<h2>Contact Us</h2>
									<ul>
										<li><a href="#" class="fa fa-twitter solo"><span>Twitter</span></a></li>
										<li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
										<li><a href="#" class="fa fa-dribbble solo"><span>Dribbble</span></a></li>
										<li><a href="#" class="fa fa-google-plus solo"><span>Google+</span></a></li>
									</ul>
									<p>1234 Fictional Road Suite #5432<br />
									Nashville, Tennessee 00000-0000<br />
									(800) 555-0000</p>
								</section>
						
						</div>
					</div>
					<div class="row">
						<div class="12u">
							<div id="copyright">
								&copy; Untitled. All rights reserved. | Images: <a href="http://fotogrph.com/">fotogrph</a> | Design: <a href="http://html5up.net/">HTML5 UP</a>
							</div>
						</div>
					</div>
				</footer>
			</div>

	</body>
</html>