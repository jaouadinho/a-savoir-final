<?php
include('config.php')
?>

<!DOCTYPE HTML>
<!--
    Verti 2.5 by HTML5 UP
    html5up.net | @n33co
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>Liste des savoirs</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,800" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Oleo+Script:400" rel="stylesheet" type="text/css" />
        <script src="js/jquery.min.js"></script>
        <script src="js/config.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-panels.min.js"></script>
        <noscript>
            <link rel="stylesheet" href="css/skel-noscript.css" />
            <link rel="stylesheet" href="css/style.css" />
            <link rel="stylesheet" href="css/style-desktop.css" />
        </noscript>
        <!--[if lte IE 8]><script src="js/html5shiv.js"></script><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
        <!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css" /><![endif]-->

            <script type="text/javascript">
 function MaxLengthTextarea(objettextarea,maxlength){
                var el = document.getElementById('insertHere');

  if (objettextarea.value.length > maxlength) {
    objettextarea.value = objettextarea.value.substring(0, maxlength);
el.innerHTML = '<div style="color:#FF4486;font-weight: 800;">Limité à 200 caractéres</div>';
   }
}
</script>
    </head>
    <body class="homepage">

        <!-- Header Wrapper -->
            <div id="header-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="12u">
                        
                            <!-- Header -->
                                <header id="header">
                                
                                    <!-- Logo -->
                                        <div id="logo">
                                            <h1><a href="index.php" id="logo">A savoir</a></h1>
                                            <span>Tu crois tout connaitre ?</span>
                                        </div>
                                    
                                    <!-- Nav -->
                                        <nav id="nav">
                                            <ul>

                                                <li class="current_page_item"><a href="liste.php">Les Savoirs</a></li>

                                                <?php
//On affiche un message de bienvenue, si lutilisateur est connecte, on affiche son pseudo
?>
<?php
//Si lutilisateur est connecte, on lui donne un lien pour modifier ses informations, pour voir ses messages et un pour se deconnecter
if(isset($_SESSION['username']))
{
?>
<li><a href="edit_infos.php">Mon profil</a></li>
            <li><a href="add.php">Ajouter un savoir</a></li>
<li><a href="connexion.php">Se d&eacute;connecter</a></li>
<?php
}
else
{
//Sinon, on lui donne un lien pour sinscrire et un autre pour se connecter
?>
<li><a href="sign_up.php">Inscription</a></li>
<li><a href="connexion.php">Se connecter</a></li>
<?php
}
?>
                                                
                                            </ul>
                                        </nav>
                                
                                </header>

                        </div>
                    </div>
                </div>
            </div>
        
        <!-- Banner Wrapper -->
            
        
        <!-- Features Wrapper -->
            <div id="features-wrapper">
                <div class="container">
                    <div class="row" >
                        
                            <!-- Box -->
<?php




    if (isset($_POST["name"]) && !empty($_POST["name"])) 
        {        

            $maxsize=1000000;

            $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
            //1. strrchr renvoie l'extension avec le point (« . »).
            //2. substr(chaine,1) ignore le premier caractère de chaine.
            //3. strtolower met l'extension en minuscules.
            $extension_upload = strtolower(  substr(  strrchr($_FILES['file']['name'], '.')  ,1)  );

            if ( in_array($extension_upload,$extensions_valides) && ($_FILES['file']['size'] < $maxsize) ) 
            {
                $uploaddir = 'upload/';
                $uploadfile = $uploaddir . basename($_FILES['file']['name']);

$link=$_FILES['file']['name'];
$text=$_POST['name'];
$id=$_GET['id'];

                echo '<pre>';



                if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) 
                {


                    echo "<h2>Le fichier est valide, et a été téléchargé
                           avec succès</h2>".$id;

                $savoir = "UPDATE savoir SET link = '$link', text = '$text'  WHERE id = '$id'";

                mysql_query($savoir);
   


                } else 
                {
                    echo "Attaque potentielle par téléchargement de fichiers.
                          Voici plus d'informations :\n";
                }

                echo '</pre>';

        ?>
                                <div class="4u">

               <section class="box box-feature">

                        <a class="image image-full"><img src="<?php echo  "upload/".$_FILES['file']['name']; ?>" height="350px" > </a>
                       
                       <div class="inner">
                                                <header>
                <h2><?php echo $_POST['name']; ?></h2>

          <?php 
        }

          else 
          {
            echo "Votre fichier n'a pas été uploadé
            Veuillez respecter les extensions JPG/png
            Votre fichier doit peser moins de 1 mo
            ";


            }  
        }
        else
        {
 
            $req = mysql_query('SELECT link,text FROM savoir WHERE id="'.$_GET['id'].'"');

            $id=$_GET['id'];
            while($dnn = mysql_fetch_array($req))
            {
                ?>
                <div class="4u" style="margin-top:20px;">

              <?php
                //Si lutilisateur est connecte, on lui donne un lien pour modifier ses informations, pour voir ses messages et un pour se deconnecter
                if(isset($_SESSION['username']))
                {
                    ?>
                   <form enctype="multipart/form-data" action="edit_savoir.php?id=<?php echo $id; ?>" method="post" accept-charset="utf-8">
    <label for="file">Choisissez une image (jpg/jpeg/gif/png) </label><br><input type="file" name="file" value="" id="file"><br><br>
    <label for="name">Ecrivez une légende </label><br><textarea onkeyup="javascript:MaxLengthTextarea(this, 200);" type="text" name="name" value="" id="name"><?php echo $dnn['text']; ?></textarea>
    <span id="insertHere"></span>
    <input type="hidden" name="userid" value="<?php echo $_SESSION['userid']; ?>" id="userid">
    
    
    <p><input type="submit" value="Envoyer &rarr;"></p>
</form>

                    <?php
                }
                else
                {
                    //Sinon, on lui donne un lien pour sinscrire et un autre pour se connecter
                    ?>
                    <li><a href="sign_up.php">Inscription</a></li>
                    <li><a href="connexion.php">Se connecter</a></li>
                    <?php
                }
            ?>

                                    </div>


            <?php
            }
        }
?>

                      
                        
                        
                    </div>
                </div>
            </div>
<div id="banner-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="12u">
                        
                            <!-- Banner -->
                                <div id="banner" class="box">

                                    <div>
                                        <div class="row">
                                            <div class="7u">
                                                <h2>C'est ton tour</h2>
                                                <p>Partage ton savoir</p>
                                            </div>
                                            <div class="5u">
                                                <ul>
                                                    <li><a href="add.php" class="button big fa fa-arrow-circle-right">Ok let's go</a></li>
                                                    <li><a href="#" class="button alt big fa fa-question-circle">Plus d'infos</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        <!-- Footer Wrapper -->
            <div id="footer-wrapper">
                <footer id="footer" class="container">
                    <div class="row">
                    
                        <div class="3u">
                        
                            <!-- Contact -->
                                <section class="widget-contact last">
                                    <h2>Contact Us</h2>
                                    <ul>
                                        <li><a href="#" class="fa fa-twitter solo"><span>Twitter</span></a></li>
                                        <li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
                                        <li><a href="#" class="fa fa-dribbble solo"><span>Dribbble</span></a></li>
                                        <li><a href="#" class="fa fa-google-plus solo"><span>Google+</span></a></li>
                                    </ul>
                                    <p>20 rue Merlin de Thionville<br />
                                    92150, Suresnes<br />
                                    0647464176</p>
                                </section>
                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="12u">
                            <div id="copyright">
                                |&copy; A savoir. All rights reserved. | 
                            </div>
                        </div>
                    </div>
                </footer>
            </div>

    </body>
</html>