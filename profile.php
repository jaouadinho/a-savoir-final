<?php
include('config.php')
?>

<!DOCTYPE HTML>
<!--
        Verti 2.5 by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
        <head>
                <title>A savoir - Profil users</title>
                <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                <meta name="description" content="" />
                <meta name="keywords" content="" />
                <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,800" rel="stylesheet" type="text/css" />
                <link href="http://fonts.googleapis.com/css?family=Oleo+Script:400" rel="stylesheet" type="text/css" />
                <script src="js/jquery.min.js"></script>
                <script src="js/config.js"></script>
                <script src="js/skel.min.js"></script>
                <script src="js/skel-panels.min.js"></script>
                <noscript>
                        <link rel="stylesheet" href="css/skel-noscript.css" />
                        <link rel="stylesheet" href="css/style.css" />
                        <link rel="stylesheet" href="css/style-desktop.css" />
                </noscript>
                <!--[if lte IE 8]><script src="js/html5shiv.js"></script><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
                <!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css" /><![endif]-->
        </head>
        <body class="no-sidebar">

                <!-- Header Wrapper -->
                        <div id="header-wrapper">
                                <div class="container">
                                        <div class="row">
                                                <div class="12u">
                                                
                                                        <!-- Header -->
                                                                <header id="header">
                                                                
                                                                        <!-- Logo -->
                                                                                <div id="logo">
                                                                                <h1><a href="index.php" id="logo">A savoir</a></h1>
                                                                                        <span>Tu crois tout connaitre ?</span>
                                                                                </div>
                                                                        
                                                                        <!-- Nav -->
                                                                        <nav id="nav">
                                                                                        <ul>


                                                                                                <?php
//On affiche un message de bienvenue, si lutilisateur est connecte, on affiche son pseudo
?>
<?php
//Si lutilisateur est connecte, on lui donne un lien pour modifier ses informations, pour voir ses messages et un pour se deconnecter
if(isset($_SESSION['username']))
{
?>
<li class="current_page_item"><a href="index.php">Administration</a></li>
<li><a href="edit_infos.php">Mon profil</a></li>
            <li><a href="add.php">Ajouter un savoir</a></li>
<li><a href="connexion.php">Se d&eacute;connecter</a></li>
<?php
}
else
{
//Sinon, on lui donne un lien pour sinscrire et un autre pour se connecter
?>
<li><a href="sign_up.php">Inscription</a></li>
<li><a href="connexion.php">Se connecter</a></li>
<?php
}
?>
                                                                                                
                                                                                        </ul>
                                                                                </nav>
                                                                
                                                                </header>

                                                </div>
                                        </div>
                                </div>
                        </div>
                
                <!-- Main Wrapper -->
                        <div id="main-wrapper">
                                <div class="container">
                                        <div class="row">
                                                <div class="12u skel-cell-important">

                                                        <!-- Content -->
                                                                <div id="content">

<?php
//On verifie que lidentifiant de lutilisateur est defini
if(isset($_GET['id']))
{
        $id = intval($_GET['id']);
        //On verifie que lutilisateur existe
        $dn = mysql_query('select username, email, avatar, signup_date from users where id="'.$id.'"');
        if(mysql_num_rows($dn)>0)
        {
                $dnn = mysql_fetch_array($dn);
                //On affiche les donnees de lutilisateur
?>
Voici le profil de "<?php echo htmlentities($dnn['username']); ?>" :

        <h1><?php echo htmlentities($dnn['username'], ENT_QUOTES, 'UTF-8'); ?></h1>
        Email: <?php echo htmlentities($dnn['email'], ENT_QUOTES, 'UTF-8'); ?><br />
        Cet utilisateur s'est inscrit le <?php echo date('d/m/Y',$dnn['signup_date']); ?></td>
   
<?php
        }
        else
        {
                echo 'Cet utilisateur n\'existe pas.';
        }
}
else
{
        echo 'L\'identifiant de l\'utilisateur n\'est pas d&eacute;fini.';
}
?>


                                                                        </article>
                                                                </div>

                                                </div>
                                        </div>
                                </div>
                        </div>

                <!-- Footer Wrapper -->
                        <div id="footer-wrapper">
                                <footer id="footer" class="container">
                                        <div class="row">
                                                
                                                <div class="3u">
                                                
                                                        <!-- Contact -->
                                                                <section class="widget-contact last">
                                                                        <h2>Contact Us</h2>
                                                                        <ul>
                                                                                <li><a href="#" class="fa fa-twitter solo"><span>Twitter</span></a></li>
                                                                                <li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
                                                                                <li><a href="#" class="fa fa-dribbble solo"><span>Dribbble</span></a></li>
                                                                                <li><a href="#" class="fa fa-google-plus solo"><span>Google+</span></a></li>
                                                                        </ul>
                                                                        <p>1234 Fictional Road Suite #5432<br />
                                                                        Nashville, Tennessee 00000-0000<br />
                                                                        (800) 555-0000</p>
                                                                </section>
                                                
                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="12u">
                                                        <div id="copyright">
                                                                &copy; Untitled. All rights reserved. | Images: <a href="http://fotogrph.com/">fotogrph</a> | Design: <a href="http://html5up.net/">HTML5 UP</a>
                                                        </div>
                                                </div>
                                        </div>
                                </footer>
                        </div>

        </body>
</html>
               