<?php
include('config.php')
?>

<!DOCTYPE HTML>
<!--
    Verti 2.5 by HTML5 UP
    html5up.net | @n33co
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>A savoir - Ton profil</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,800" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Oleo+Script:400" rel="stylesheet" type="text/css" />
        <script src="js/jquery.min.js"></script>
        <script src="js/config.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-panels.min.js"></script>
        <noscript>
            <link rel="stylesheet" href="css/skel-noscript.css" />
            <link rel="stylesheet" href="css/style.css" />
            <link rel="stylesheet" href="css/style-desktop.css" />
        </noscript>
        <!--[if lte IE 8]><script src="js/html5shiv.js"></script><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
        <!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css" /><![endif]-->
    </head>
    <body class="no-sidebar">

        <!-- Header Wrapper -->
            <div id="header-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="12u">
                        
                            <!-- Header -->
                                <header id="header">
                                
                                    <!-- Logo -->
                                        <div id="logo">
                                        <h1><a href="index.php" id="logo">A savoir</a></h1>
                                            <span>Tu crois tout connaitre ?</span>
                                        </div>
                                    
                                    <!-- Nav -->
                                    <nav id="nav">
                                            <ul>

                                                <li ><a href="liste.php">Les Savoirs</a></li>

                                                <?php
//On affiche un message de bienvenue, si lutilisateur est connecte, on affiche son pseudo
?>
<?php
//Si lutilisateur est connecte, on lui donne un lien pour modifier ses informations, pour voir ses messages et un pour se deconnecter
if(isset($_SESSION['username']))
{
?>
<li class="current_page_item"><a href="edit_infos.php" >Mon profil</a></li>
            <li><a href="add.php">Ajouter un savoir</a></li>
<li><a href="connexion.php">Se d&eacute;connecter</a></li>
<?php
}
else
{
//Sinon, on lui donne un lien pour sinscrire et un autre pour se connecter
?>
<li><a href="sign_up.php">Inscription</a></li>
<li><a href="connexion.php">Se connecter</a></li>
<?php
}
?>
                                                
                                            </ul>
                                        </nav>
                                
                                </header>

                        </div>
                    </div>
                </div>
            </div>
        
        <!-- Main Wrapper -->
            <div id="main-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="12u skel-cell-important">

                            <!-- Content -->
                                <div id="content">
<?php
//On verifie que le formulaire a ete envoye
if(isset($_POST['username'],$_POST['password'],$_POST['passverif'],$_POST['email']) and $_POST['username']!='')
{
        //On enleve lechappement si get_magic_quotes_gpc est active
        if(get_magic_quotes_gpc())
        {
                $_POST['username'] = stripslashes($_POST['username']);
                $_POST['password'] = stripslashes($_POST['password']);
                $_POST['passverif'] = stripslashes($_POST['passverif']);
                $_POST['email'] = stripslashes($_POST['email']);
                $_POST['avatar'] = stripslashes($_POST['avatar']);
        }
        //On verifie si le mot de passe et celui de la verification sont identiques
        if($_POST['password']==$_POST['passverif'])
        {
                //On verifie si le mot de passe a 6 caracteres ou plus
                if(strlen($_POST['password'])>=6)
                {
                        //On verifie si lemail est valide
                        if(preg_match('#^(([a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+\.?)*[a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+)@(([a-z0-9-_]+\.?)*[a-z0-9-_]+)\.[a-z]{2,}$#i',$_POST['email']))
                        {
                                //On echape les variables pour pouvoir les mettre dans une requette SQL
                                $username = mysql_real_escape_string($_POST['username']);
                                $password = mysql_real_escape_string($_POST['password']);
                                $email = mysql_real_escape_string($_POST['email']);
                                $avatar = mysql_real_escape_string($_POST['avatar']);
                                //On verifie sil ny a pas deja un utilisateur inscrit avec le pseudo choisis
                                $dn = mysql_num_rows(mysql_query('select id from users where username="'.$username.'"'));
                                if($dn==0)
                                {
                                        //On recupere le nombre dutilisateurs pour donner un identifiant a lutilisateur actuel
                       
                           
                           $user=$_POST['username'];  
                           $id=$_SESSION['userid'];           //On enregistre les informations dans la base de donnee

$sa = "UPDATE users SET username = '$user',password = '$password',email = '$email' WHERE id = '$id'";

         
                
                                        if( mysql_query($sa))
                                        	
                                        {
                                                //Si ca a fonctionne, on naffiche pas le formulaire
                                                $form = false;
?>
<div class="message">Vous avez bien &eacute;t&eacute; inscrit. Vous pouvez dor&eacute;navant vous connecter.<br />
<a href="connexion.php">Se connecter</a></div>
<?php
                                        }
                                        else
                                        {
                                                //Sinon on dit quil y a eu une erreur
                                                $form = true;
                                                $message = 'Une erreur est survenue lors de l\'inscription.';
                                        }
                                }
                                else
                                {
                                        //Sinon, on dit que le pseudo voulu est deja pris
                                        $form = true;
                                        $message = 'Un autre utilisateur utilise d&eacute;j&agrave; le nom d\'utilisateur que vous d&eacute;sirez utiliser.';
                                }
                        }
                        else
                        {
                                //Sinon, on dit que lemail nest pas valide
                                $form = true;
                                $message = 'L\'email que vous avez entr&eacute; n\'est pas valide.';
                        }
                }
                else
                {
                        //Sinon, on dit que le mot de passe nest pas assez long
                        $form = true;
                        $message = 'Le mot de passe que vous avez entr&eacute; contien moins de 6 caract&egrave;res.';
                }
        }
        else
        {
                //Sinon, on dit que les mots de passes ne sont pas identiques
                $form = true;
                $message = 'Les mots de passe que vous avez entr&eacute; ne sont pas identiques.';
        }
}
else
{
        $form = true;
}
if($form)
{
        //On affiche un message sil y a lieu
        if(isset($message))
        {
                echo '<div class="message">'.$message.'</div>';
        }
        //On affiche le formulaire

?>
<div class="content">
    <form action="edit_infos.php" method="post">
       <h2>Modifier mes informations:</h2><br />
        <div class="center">
            <label for="username">Nom d'utilisateur</label><input type="text" name="username" value="" /><br />
            <label for="password">Mot de passe<span class="small">(6 caract&egrave;res min.)</span></label><input type="password" name="password" /><br />
            <label for="passverif">Mot de passe<span class="small">(v&eacute;rification)</span></label><input type="password" name="passverif" /><br />
            <label for="email">Email</label><input type="text" name="email" value="<?php if(isset($_POST['email'])){echo htmlentities($_POST['email'], ENT_QUOTES, 'UTF-8');} ?>" /><br />
            <input type="submit" value="Envoyer" />
                </div>


                <style>

                </style>
    </form>
</div>
<?php
}
?>

<h2 style="margin-top:70px;">Mes participations</h2>

<?php

$req = mysql_query('SELECT id,link,text FROM savoir WHERE  user_id="'.$_SESSION['userid'].'"');


while($dnn = mysql_fetch_array($req))
{
?>
       <div style="background:#FF4486;padding:10px;color:white;border-radius: 6px;  word-wrap: break-word;">
                <img src="<?php echo "upload/".$dnn['link']; ?>" width="50" > 

           

        <?php echo htmlentities($dnn['text'], ENT_QUOTES, 'UTF-8'); ?><br>
        <a style="border-radius: 6px;
    font-weight: 800;
    letter-spacing: 0.025em;
    padding: 0.5em 1em;
     background: none repeat scroll 0 0 #444444;
    color: #FFFFFF;
    text-decoration: none;
    transition: background-color 0.25s ease-in-out 0s;"href="voir_savoir.php?id=<?php echo $dnn['id']; ?> " >Voir</a>&nbsp;<a style="border-radius: 6px;
    font-weight: 800;
    letter-spacing: 0.025em;
    padding: 0.5em 1em;
     background: none repeat scroll 0 0 #444444;
    color: #FFFFFF;
    text-decoration: none;
    transition: background-color 0.25s ease-in-out 0s;"href="edit_savoir.php?id=<?php echo $dnn['id']; ?> " >Edit</a>&nbsp;<a style="border-radius: 6px;
    font-weight: 800;
    letter-spacing: 0.025em;
    padding: 0.5em 1em;
     background: none repeat scroll 0 0 #444444;
    color: #FFFFFF;
    text-decoration: none;
    transition: background-color 0.25s ease-in-out 0s;" href="delete_savoir.php?id=<?php echo $dnn['id']; ?> " >X</a>&nbsp;<br></div>
<?php
}
?>
  

                                    </article>
                                </div>

                        </div>
                    </div>
                </div>
            </div>

       
        <!-- Footer Wrapper -->
            <div id="footer-wrapper">
                <footer id="footer" class="container">
                    <div class="row">
                    
                        <div class="3u">
                        
                            <!-- Contact -->
                                <section class="widget-contact last">
                                    <h2>Contact Us</h2>
                                    <ul>
                                        <li><a href="#" class="fa fa-twitter solo"><span>Twitter</span></a></li>
                                        <li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
                                        <li><a href="#" class="fa fa-dribbble solo"><span>Dribbble</span></a></li>
                                        <li><a href="#" class="fa fa-google-plus solo"><span>Google+</span></a></li>
                                    </ul>
                                    <p>20 rue Merlin de Thionville<br />
                                    92150, Suresnes<br />
                                    0647464176</p>
                                </section>
                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="12u">
                            <div id="copyright">
                                |&copy; A savoir. All rights reserved. | 
                            </div>
                        </div>
                    </div>
                </footer>
            </div>

    </body>
</html>