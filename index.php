<?php
include('config.php')
?>

<!DOCTYPE HTML>
<!--
	Verti 2.5 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>A savoir</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,800" rel="stylesheet" type="text/css" />
		<link href="http://fonts.googleapis.com/css?family=Oleo+Script:400" rel="stylesheet" type="text/css" />
		<script src="js/jquery.min.js"></script>
		<script src="js/config.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
		</noscript>
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
		<!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css" /><![endif]-->
	</head>
	<body class="homepage">

		<!-- Header Wrapper -->
			<div id="header-wrapper">
				<div class="container">
					<div class="row">
						<div class="12u">
						
							<!-- Header -->
								<header id="header">
								
									<!-- Logo -->
										<div id="logo">
											<h1><a href="#" id="logo">A savoir</a></h1>
											<span><a href="nimda.php" style="color:#696969;text-decoration:none;">Tu crois tout connaitre ?</a></span>
										</div>
									
									<!-- Nav -->
										<nav id="nav">
											<ul>

												<li ><a href="liste.php">Les Savoirs</a></li>

												<?php
//On affiche un message de bienvenue, si lutilisateur est connecte, on affiche son pseudo
?>
<?php
//Si lutilisateur est connecte, on lui donne un lien pour modifier ses informations, pour voir ses messages et un pour se deconnecter
if(isset($_SESSION['username']))
{
?>
<li><a href="edit_infos.php">Mon profil</a></li>
            <li><a href="add.php">Ajouter un savoir</a></li>
<li><a href="connexion.php">Se d&eacute;connecter</a></li>
<?php
}
else
{
//Sinon, on lui donne un lien pour sinscrire et un autre pour se connecter
?>
<li><a href="sign_up.php">Inscription</a></li>
<li><a href="connexion.php">Se connecter</a></li>
<?php
}
?>
												
											</ul>
										</nav>
								
								</header>

						</div>
					</div>
				</div>
			</div>
		
		<!-- Banner Wrapper -->
			<div id="banner-wrapper">
				<div class="container">
					<div class="row">
						<div class="12u">
						
							<!-- Banner -->
								<div id="banner" class="box">

									<div>
										<div class="row">
											<div class="7u">
												<h2>C'est ton tour</h2>
												<p>Partage ton savoir</p>
											</div>
											<div class="5u">
												<ul>
													<li><a href="add.php" class="button big fa fa-arrow-circle-right">Ok let's go</a></li>
													<li><a href="#main-wrapper" class="button alt big fa fa-question-circle">Plus d'infos</a></li>
												</ul>
											</div>
										</div>
									</div>
								
								</div>

						</div>
					</div>
				</div>
			</div>
		
		<!-- Features Wrapper -->
			<div id="features-wrapper">
				<div class="container">
					<div class="row">
						
							<!-- Box -->
								
								<?php


//On recupere les identifiants, les pseudos et les emails des utilisateurs
$req = mysql_query('SELECT link,text FROM savoir WHERE 1 ORDER BY RAND() LIMIT 3 ');


while($dnn = mysql_fetch_array($req))
{
?>
						<div class="4u">

       <section class="box box-feature">
                <a class="image image-full"><img src="<?php echo "upload/".$dnn['link']; ?>" height="350px" > </a>
               <div class="inner">
										<header>
        <h2><?php echo htmlentities($dnn['text'], ENT_QUOTES, 'UTF-8'); ?></h2>

        											
										</header>
										
									</div>
								</section>

						</div>
    
<?php
}
?>
									
						
						
						
					</div>
				</div>
			</div>

		<!-- Main Wrapper -->
			<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<div class="4u">
						
							<!-- Sidebar -->
								<div id="sidebar">
									<section class="widget-thumbnails">
										<h3>Mes autres projets</h3>
										<div class="grid">
											<div class="row no-collapse half">
												<div class="6u"><a href="http://lo-architectes.com/" class="image image-full"><img src="http://lo-architectes.com/wp-content/themes/architect/img/logofinale.png" alt="" /></a></div>
												<div class="6u"><a href="http://bloggybetty.com/" class="image image-full"><img src="http://bloggybetty.com/wp-content/themes/betty/img/logo.png" alt="" /></a></div>
												
											</div>
										</div>
										<a href="http://stedau.com/" class="button fa fa-file-text-o">Plus ?</a>
									</section>
								</div>
						
						</div>
						<div class="8u">

							<!-- Content -->
								<div id="content">
									<section class="last">
										<h2>Le concept enfaite ?</h2>
										<p>C'est assez simple, tu connais quelque chose sur quelqu'un ou quelque chose ? 
											<p>Holala trop de quelque, Bref en gros tu as juste à partager ton SAVOIR comme dirait notre tres chér Eddy Malou philosophe du 21émes siécles.</p>
										<p>	Pour ca tu choisis une photos et tu met ton texte et il serra uploadé sur le site automatiquement... Attention si tu mets des photos bisard t'ettonne pas si tu te fais BAN, ALLEZ SALUT</p>
										<a href="contact.php" class="button fa fa-arrow-circle-right">J'ai rien compris</a>
									</section>
								</div>

						</div>
					</div>
				</div>
			</div>

		<!-- Footer Wrapper -->
			<div id="footer-wrapper">
				<footer id="footer" class="container" >
					<div class="row">
					
						<div class="3u">
						
							<!-- Contact -->
								<section class="widget-contact last" style="margin:0 auto; width:70%;">
									<h2>Contact Us</h2>
									<ul>
										<li><a href="#" class="fa fa-twitter solo"><span>Twitter</span></a></li>
										<li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
										<li><a href="#" class="fa fa-dribbble solo"><span>Dribbble</span></a></li>
										<li><a href="#" class="fa fa-google-plus solo"><span>Google+</span></a></li>
									</ul>
									<p>20 rue Merlin de Thionville<br />
									92150, Suresnes<br />
									0647464176</p>
								</section>
						
						</div>
					</div>
					<div class="row">
						<div class="12u">
							<div id="copyright">
								|&copy; A savoir. All rights reserved. | 
							</div>
						</div>
					</div>
				</footer>
			</div>

	</body>
</html>